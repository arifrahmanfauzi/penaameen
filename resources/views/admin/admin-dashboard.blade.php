@extends('layouts.app')
@section('css')

@endsection
@section('header')
<h5 class="m-b-10">Dashboard</h5>
<p class="m-b-0">Welcome to Mega Able</p>
@endsection

@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="index.html"> <i class="fa fa-home"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Dashboard</a>
    </li>
</ul>
@endsection
@section('content')
@if (session('status'))
<div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong> {{ session('status') }} </strong>
</div>
@endif
<div class="row">
    <div class="col-xl-3 col-md-6">
        <div class="card">
            <div class="card-block">
                <div class="row align-items-center">
                    <div class="col-8">
                      @php
                        $data = DB::table('tb_mitra')->count();
                      @endphp
                        <h4 class="text-c-purple">{{ $data }}</h4>
                        <h6 class="text-muted m-b-0">Total Mitra</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="fa fa-bar-chart f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-purple">
                <div class="row align-items-center">
                    <div class="col-9">
                        <p class="text-white m-b-0">% change</p>
                    </div>
                    <div class="col-3 text-right">
                        <i class="fa fa-line-chart text-white f-16"></i>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card">
            <div class="card-block">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-green">0000</h4>
                        <h6 class="text-muted m-b-0">Page</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="fa fa-file-text-o f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-green">
                <div class="row align-items-center">
                    <div class="col-9">
                        <p class="text-white m-b-0">% change</p>
                    </div>
                    <div class="col-3 text-right">
                        <i class="fa fa-line-chart text-white f-16"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-12 col-md-12">
      <div class="card">
        <div class="card-header">

        </div>
        <div class="card-block">

        </div>
      </div>
    </div>
</div>
@endsection
@section('javascript')

@endsection
