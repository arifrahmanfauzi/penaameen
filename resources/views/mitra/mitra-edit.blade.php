@extends('layouts.app')
@section('css')

@endsection
@section('header')
<h5>Edit Mitra</h5>
@endsection
@section('breadcumb')

@endsection
@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="card">
      <div class="card-header">
        <h5>Edit Data Mitra</h5>
      </div>
      <div class="card-block">
        @forelse ($data as $nilai)
          <form action="{{ route('mitra.update',$nilai->id_mitra) }}" method="post">
            @method('PUT')
            @csrf

            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama</label>
              <div class="col-sm-5">
                <input type="text" name="nama" class="edit form-control" value="{{ $mitra->nama_mitra }}">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Alamat</label>
              <div class="col-sm-5">
                <input type="text" name="alamat" class="edit form-control" value="{{ $mitra->alamat }}">
              </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">NIK</label>
                <div class="col-sm-5">
                    <input type="text" name="nik" class="form-control" value="{{ $mitra->nik }}">
                </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Trainer</label>
              <div class="col-sm-5">
                <select class="custom-select" name="is_trainer">
                  {{-- <option value="{{ $mitra->trainer }}" selected>{{ $mitra->trainer }}</option> --}}
                  @php
                    $ya = 'ya';
                    $tidak = 'tidak';

                  @endphp
                  <option value="{{$ya}}" @if ( $ya == $mitra->trainer) selected @endif>{{$ya}}</option>
                  <option value="{{$tidak}}" @if ($tidak == $mitra->trainer) selected @endif>{{$tidak}}</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Kemitraan</label>
              <div class="col-sm-5">
                <select class="custom-select" name="jenis_mitra">
                  @foreach ($jenismitra as $jenismitra)
                    <option value="{{ $jenismitra->id_jenis_mitra }}">{{ $jenismitra->nama_jenis_mitra }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nomor Anggota</label>
                <div class="col-sm-5">

                    <input type="text" name="nomor_anggota" class="form-control" value="{{ $mitra->nomor_anggota }}">
                </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-from-label">Upload Foto</label>
              <div class="col-sm-5">
                  <input type="file" class="form-control-file" name="photo" id="avatarFile" aria-describedby="fileHelp">
                  <small id="fileHelp" class="form-text text-muted">resolisi di atas 400 x 400. ukuran max 2MB.</small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
              <div class="col-sm-5">
                <select class="custom-select" name="jenis_kelamin">
                  <option value="laki-laki">laki - laki</option>
                  <option value="perempuan">perempuan</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tempat Tgl Lahir</label>
              <div class="col-sm-4">
                <select id="kotalahir" class="chosen custom-select" name="kotalahir" required>
                  @php
                  $kota = DB::table('kabupaten')->get();
                  @endphp
                  @foreach ($kota as $kota)
                    <option value="{{ $kota->IDKabupaten }}" @if ($mitra->kota_lahir == $kota->IDKabupaten)
                      selected
                    @endif>{{ $kota->Nama }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4">
                <input type="date" name="tgl_lahir" value="{{ $mitra->tgl_lahir }}" class="form-control">
              </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tempat Tinggal/Alamat Surat</label>
                @foreach ($data as $datas)
                  <div class="col-sm-4">
                    <textarea type="text" name="alamat" class="form-control">{{ $mitra->alamat }}</textarea>
                  </div>
                  <div class="col-sm-1">
                    <input type="text" name="rw" placeholder="RW" class="form-control" value="{{ $mitra->rw }}">
                  </div>
                  <div class="col-sm-1">
                    <input type="text" name="rt" placeholder="RT" class="form-control" value="{{ $mitra->rt }}">
                  </div>
                  <div class="col-sm-1">
                    <input type="text" name="usia" placeholder="Usia" class="form-control" value="{{ $mitra->usia }}">
                  </div>
                @endforeach



                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label"></label>
                  <div class="col-sm-9">

                    <select id="provinsi" class="custom-select" name="provinsi">
                      @foreach ($wilayah as $kota)
                        <option value="{{ $kota->IDProvinsi }}" @if ($kota->IDProvinsi == $mitra->id_provinsi)
                          selected
                        @endif>{{ $kota->Nama }}</option>
                      @endforeach
                    </select>
                    @php
                      $datakota = DB::table('kabupaten')->get();
                      $datakecamatan = DB::table('kecamatan')->where('IDKabupaten',$mitra->id_kabupaten)->get();
                      $datakelurahan = DB::table('kelurahan')->where('IDKecamatan',$mitra->id_kecamatan)->get();
                    @endphp
                    <select id="kota" class="custom-select" name="kota">
                      @foreach ($datakota as $dkota)
                        <option value="{{ $dkota->IDKabupaten }}" @if ($dkota->IDKabupaten == $kabupaten->IDKabupaten) selected @endif>{{ $dkota->Nama }}</option>
                      @endforeach
                    </select>
                    <select id="kecamatan" class="custom-select" name="kecamatan">
                      @foreach ($datakecamatan as $dkecamatan)
                        <option value="{{ $dkecamatan->IDKecamatan }}" @if ($dkecamatan->IDKecamatan == $kecamatan->IDKecamatan) selected @endif>{{ $dkecamatan->Nama }}</option>
                      @endforeach
                    </select>
                    <select id="kelurahan" class="custom-select" name="kelurahan">
                      @foreach ($datakelurahan as $dkelurahan)
                        <option value="{{ $dkelurahan->IDKelurahan }}" @if ($dkelurahan->IDKelurahan == $kelurahan->IDKelurahan) selected @endif>{{ $dkelurahan->Nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Status Perkawinan</label>
                <div class="col-sm-5">
                    <select class="custom-select" name="status_kawin">
                      <option value="sudah">sudah</option>
                      <option value="belum">belum</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Status Warga Negara</label>
                <div class="col-sm-5">
                    <input type="text" name="status_warga" class="form-control" value="{{ $mitra->warga_negara }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Agama</label>
                <div class="col-sm-5">
                    <select class="custom-select" name="agama">
                      <option value="islam">islam</option>
                      <option value="kristen">kristen</option>
                      <option value="khatolik">khatolik</option>
                      <option value="protestan">protenstan</option>
                      <option value="hindu">hindu</option>
                      <option value="budha">budha</option>
                      <option value="konghucu">konghucu</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Suku</label>
                <div class="col-sm-5">
                    <input type="text" name="suku" class="form-control" value="{{ $mitra->suku }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jumlah Anak Laki -Laki</label>
                <div class="col-sm-5">
                    <input type="text" name="jumlah_l" class="form-control" value="{{ $mitra->jumlah_anak_l }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jumlah Anak Perempuan</label>
                <div class="col-sm-5">
                    <input type="text" name="jumlah_p" class="form-control" value="{{ $mitra->jumlah_anak_p }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nomer Telp</label>
                <div class="col-sm-5">
                    <input type="text" name="nomor" class="form-control" value="{{ $mitra->nomor_telp }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Email</label>
                <div class="col-sm-5">
                    <input type="email" name="email" class="form-control" value="{{ $mitra->email }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pendidikan</label>
                <div class="col-sm-5">
                  <select id="pendidikan" class="custom-select" name="pendidikan">
                        <option value="sarjana">Sarjana</option>
                        <option value="tidak tamat sd">Tidak tamat sd</option>
                        <option value="sd">sd</option>
                        <option value="smp">smp</option>
                        <option value="sma">sma</option>
                        <option value="lain">lain-lain</option>
                      </select>
                      <input id="other" type="text" name="other" class="form-control" style="width:50%" hidden>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Pekerjaan</label>
                <div class="col-sm-5">
                    <input type="text" name="pekerjaan" class="form-control" value="{{ $mitra->pekerjaan }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">NIP</label>
                <div class="col-sm-5">
                    <input type="text" name="nip" class="form-control" value="{{ $mitra->nip }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">NPWP</label>
                <div class="col-sm-5">
                    <input type="text" name="npwp" class="form-control" value="{{ $mitra->npwp }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Status Kepegawaian</label>
                <div class="col-sm-5">
                    <input type="text" name="status_pegawai" class="form-control" value="{{ $mitra->status_kepegawaian }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Lembaga/Instansi</label>
                <div class="col-sm-5">
                    <input type="text" name="lembaga" class="form-control" value="{{ $mitra->nama_lembaga }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">No.Telp Lembaga</label>
                <div class="col-sm-5">
                    <input type="text" name="telp_lembaga" class="form-control" value="{{ $mitra->tlp_lembaga }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Alamat Lembaga</label>
                <div class="col-sm-5">
                    <input type="text" name="alamat_lembaga" class="form-control" value="{{ $mitra->alamat_lembaga }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jabatan</label>
                <div class="col-sm-5">
                    <input type="text" name="jabatan" class="form-control" value="{{ $mitra->jabatan }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Deskripsi</label>
                <div class="col-sm-5">
                    <textarea class="form-control" name="deskripsi"></textarea>
                </div>
            </div>
            <button type="submit" name="button">save</button>
          </form>
        @empty
          <div class="alert alert-danger">
              <strong>Data Masih Kosong !</strong>
          </div>
        @endforelse
      </div>

    </div>
  </div>

</div>

@endsection
@section('javascript')
<script>

$('#provinsi').change(function(){
  var idprovinsi = $(this).val();
  //console.log(idprovinsi);
  if (idprovinsi) {
    $.ajax({
      type:"GET",
      url:"{{ url('wilayah/kota') }}?idprovinsi="+idprovinsi,
      success: function(data){
        //console.log(data);
        $('#kota').empty();
        $("#kota").append('<option></option>');
        $.each(data,function(value,key){
           $("#kota").append('<option value="'+ key +'">'+ value +'</option>');
        });
      }
    });
  }else {
    $("#kota").empty();
  }
});
$('#kota').change(function() {
  var idkota = $(this).val();
  //console.log(idkota);
  if (idkota) {
    $.ajax({
      type:"GET",
      url:"{{ url('wilayah/kecamatan') }}?idkota="+idkota,
      success:function(data){
        $('#kecamatan').empty();
        $('#kecamatan').append('<option></option>');
        $.each(data,function(value,key){
          $('#kecamatan').append('<option value="'+key+'">'+value+'</option>');
        });
      }
    });
  }else {
    $('#kecamatan').empty();
  }
});
$('#kecamatan').change(function(event) {
  var idkecamtan = $(this).val();
  if (idkecamtan) {
    $.ajax({
      type:"GET",
      url:"{{ url('wilayah/kelurahan') }}?idkecamatan="+idkecamtan,
      success:function(data){
        $('#kelurahan').empty();
        $('#kelurahan').append('<option></option>');
        $.each(data,function(value,key){
          $('#kelurahan').append('<option value="'+key+'">'+value+'</option>');
        });
      }
    });
  }else {

  }
});

</script>
@endsection
