@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-header">
          <h5>Data GMF</h5>
        </div>
        <div class="card-block">
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Terang</label>
              <div class="col-sm-5">
                  <input type="text" name="nama" class="form-control" value="{{ $gmf->nama }}" disabled>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">NIK</label>
              <div class="col-sm-5">
                  <input type="text" name="nik" class="form-control" value="{{ $gmf->nik }}" disabled>
              </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-5">
              <input type="text" name="" class="form-control" value="{{ $gmf->jenis_kelamin }}" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Tempat Tinggal</label>
            <div class="col-sm-5">
              <input type="text" name="" class="form-control" value="{{ $gmf->Nama }}" disabled>
            </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status Perkawinan</label>
              <div class="col-sm-5">
                  <input type="text" name="" class="form-control" value="{{ $gmf->status_kawin }}" disabled>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Status Warga Negara</label>
              <div class="col-sm-5">
                  <input type="text" name="status_warga" class="form-control" value="{{ $gmf->warga_negara }}" disabled>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card">
        <div class="col-md-12">
          <img src="{{ url('storage/gmfphotos/'.$gmf->photo) }}" alt="User-Profile-Image" class="img-150" style="width:100%">
          <div class="card-block">


          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
