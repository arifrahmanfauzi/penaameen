@extends('layouts.app')
@section('css')

@endsection
@section('header')
<h5>Data Siswa</h5>
@endsection
@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="{{ url('/') }}"> <i class="fa fa-home"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Data Siswa</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if (session('status-siswa'))
        <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> {{ session('status-siswa') }} </strong>
        </div>
        @endif

    <div class="card">
        <div class="card-header">
            <h5>Tambah data siswa</h5>
        </div>
        <div class="card-block">
          <div class="col-lg-12 col-xl-12">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs md-tabs" role="tablist">
                  <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#home3" role="tab">siswa kursus</a>
                      <div class="slide"></div>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#settings3" role="tab">siswa kursus dewasa</a>
                      <div class="slide"></div>
                  </li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content card-block">
                  <div class="tab-pane active" id="home3" role="tabpanel">
                    <form class="" action="{{ route('siswa.store') }}" method="post">
                      @csrf

                    <!-- <form action="/siswa/create" method="POST">
                    {{csrf_field()}} -->
              <div class="card-block" style="background-color:rgb(225, 231, 237, 0.3);">
                <h6>formulir siswa :</h6>
              <div class="form-group row">
                <label  class="col-sm-2 col-form-label">Nama Siswa</label>
                <div class="col-sm-10">
                  <input name="nama" type="text" class="form-control" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-10">
                  <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
                    <option value="laki - laki" >Laki Laki</option>
                    <option value="perempuan" >Perempuan</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">tempat lahir</label>
                  <div class="col-sm-10">
                <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tempat lahir" >
              </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">tanggal lahir</label>
                <div class="col-sm-10">
                <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tanggal lahir" >
              </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Umur</label>
                <div class="col-sm-10">
                <input name="umur" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="umur" >
              </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Anak ke</label>
                <div class="col-sm-10">
                <input name="anak_ke" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="anak ke" >
              </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Berapa Bersaudara</label>
                <div class="col-sm-10">
                  <input name="bersaudara" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Alamat Rumah</label>
                <div class="col-sm-10">
                  <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Sekolah</label>
                <div class="col-sm-10">
                <input name="nama_sekolah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Sekolah" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kelas</label>
                <div class="col-sm-10">
                  <input name="kelas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Alamat Sekolah</label>
                <div class="col-sm-10">
                <textarea name="alamat_sekolah" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Agama</label>
                <div class="col-sm-10">
                  <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Suku</label>
                <div class="col-sm-10">
                  <input name="suku" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Bimbel Tambahan</label>
                <div class="col-sm-10">
                <input name="bimbel" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Dengan Siapa Sepulang Sekolah</label>
                <div class="col-sm-10">
                <input name="teman_dirumah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Guru Dirumah</label>
                <div class="col-sm-10">
                <input name="guru_dirumah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Apakah sepulang sekolah tidur siang</label>
                <div class="form-check">
                  <div class="col-sm-10">
                  <input class="form-check-input" type="radio" name="tidur" id="gridRadios1" value="iya" checked>
                  <label class="form-check-label" for="gridRadios1">
                    Iya
                  </label>
                </div>
                </div>
                <div class="form-check">
                  <div class="col-sm-10">
                  <input class="form-check-input" type="radio" name="tidur " id="gridRadios2" value="tidak">
                  <label class="form-check-label" for="gridRadios2">
                    Tidak
                  </label>
                </div>
              </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Bagaimana minat belajar dirumah</label>
                <div class="col-sm-10">
                  <input name="minat_belajar_dirumah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Mainan kesukaan</label>
                <div class="col-sm-10">
                <input name="mainan_kesukaan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Apakah sudah mengenal gadget</label>
                <div class="col-sm-10">
                  <input name="mengenal_gadget" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">sering bermain gadget</label>
                <div class="col-sm-10">
                  <input name="bermain_gadget" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Riwayat penyakit</label>
                <div class="col-sm-10">
                <input name="riwayat_penyakit" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Program yang dipilih</label>
                <div class="col-sm-10">
                  <select name="program_kursus" class="form-control" id="exampleFormControlSelect1">
                  <option value="ALBARQY">Al-barqy</option>
                  <option value="ACM" >Aku Cepat Membaca(ACM)</option>
                  <option value="JALPIN" >Jalpin</option>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">hari kursus</label>
                <div class="col-sm-10">
                  <input name="hari_kursus" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">waktu kursus</label>
                <div class="col-sm-10">
                  <input name="waktu_kursus" type="time" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="00:00">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Jenis kelas</label>
                <div class="col-sm-10">
                  <select name="jenis_kelas" class="form-control" id="exampleFormControlSelect1">
                  <option  value="PRIVAT">PRIVATE</option>
                  <option value="REGULER">REGULER</option>
                  <option value="KHUSUS">KHUSUS</option>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Upload foto siswa</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>

              </div>
              <br>
              <div class="card-block" style="background-color:rgb(225, 231, 237, 0.3);">
                <h6>formulir orang tua:</h6>
                <div class="form-group row">
                  <label  class="col-sm-2 col-form-label">Nama orang tua</label>
                  <div class="col-sm-10">
                    <input name="nama_ortu" type="text" class="form-control" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                  <div class="col-sm-10">
                    <select name="jenis_kelamin_ortu" class="form-control" id="exampleFormControlSelect1">
                      <option value="laki - laki" >Laki Laki</option>
                      <option value="perempuan" >Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">tempat lahir</label>
                    <div class="col-sm-10">
                  <input name="tempat_lahir_ortu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tempat lahir" >
                </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">tanggal lahir</label>
                  <div class="col-sm-10">
                  <input name="tanggal_lahir_ortu" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tanggal lahir" >
                </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Umur</label>
                  <div class="col-sm-10">
                  <input name="umur_ortu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="umur" >
                </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Alamat Rumah</label>
                  <div class="col-sm-10">
                    <textarea name="alamat_ortu" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Pendidikan ayah</label>
                  <div class="col-sm-10">
                  <input name="pendidikan_ayah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Pendidikan ibu</label>
                  <div class="col-sm-10">
                    <input name="pendidikan_ibu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Pekerjaan ayah</label>
                  <div class="col-sm-10">
                  <input name="pekerjaan_ayah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Pekerjaan ibu</label>
                  <div class="col-sm-10">
                    <input name="pekerjaan_ibu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">no telp</label>
                  <div class="col-sm-10">
                    <input name="no_telp" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Agama</label>
                  <div class="col-sm-10">
                    <input name="agama_ortu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Suku</label>
                  <div class="col-sm-10">
                    <input name="suku_ortu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                  </div>
                </div>
              </div>

                    <button type="submit" class="btn btn-primary" name"button">Save</button>
                    </form>
                  </div>


                  <!-- siswa dewasa -->
                  <div class="tab-pane" id="settings3" role="tabpanel">

                    <form class="" action="{{ url('siswa_dewasa') }}" method="post">
                      @csrf
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Nama Siswa</label>
                        <div class="col-sm-10">
                          <input name="nama" type="text" class="form-control" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                          <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
                            <option value="laki - laki">Laki Laki</option>
                            <option value="perempuan">Perempuan</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">tempat lahir</label>
                          <div class="col-sm-10">
                        <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tempat lahir" >
                      </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">tanggal lahir</label>
                        <div class="col-sm-10">
                        <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tanggal lahir" >
                      </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Umur</label>
                        <div class="col-sm-10">
                        <input name="umur" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="umur" >
                      </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Alamat Rumah</label>
                        <div class="col-sm-10">
                          <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Pendidikan</label>
                        <div class="col-sm-10">
                          <input name="pendidikan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Pekerjaan</label>
                        <div class="col-sm-10">
                          <input name="pekerjaan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">No_telepon</label>
                        <div class="col-sm-10">
                          <input name="no_telp" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Agama</label>
                        <div class="col-sm-10">
                          <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Suku</label>
                        <div class="col-sm-10">
                          <input name="suku" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Riwayat penyakit</label>
                        <div class="col-sm-10">
                        <input name="riwayat_penyakit" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Program yang dipilih</label>
                        <div class="col-sm-10">
                          <select name="program_kursus" class="form-control" id="exampleFormControlSelect1">
                          <option value="AL BARQY DEWASA">Al-barqy dewasa(jalpin)</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">hari kursus</label>
                        <div class="col-sm-10">
                          <input name="hari_kursus" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">waktu kursus</label>
                        <div class="col-sm-10">
                          <input name="waktu_kursus" type="time" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="00:00">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis kelas</label>
                        <div class="col-sm-10">
                          <select name="jenis_kelas" class="form-control" id="exampleFormControlSelect1">
                          <option  value="PRIVAT" >PRIVATE</option>
                          <option value="REGULER"  >REGULER</option>
                          <option value="KHUSUS"  >KHUSUS</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Upload foto siswa</label>
                        <div class="col-sm-10">
                          <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                        </div>

                    <button type="submit" class="btn btn-primary" name"button">Save</button>
                    </form>


                  </div>
              </div>
          </div>
        </div>
</div>


</div>
@endsection
