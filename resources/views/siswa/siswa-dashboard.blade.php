@extends('layouts.app')
@section('css')

@endsection
@section('header')
<h5>Data Siswa</h5>
@endsection
@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="{{ url('/') }}"> <i class="fa fa-home"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Data Siswa</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">
      <div class="col-6">
              <!-- button -->


      </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Data Siswa</h5>

            </div>
            <div class="card-block">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Siswa</th>
                            <th>Tempat & Tanggal lahir</th>
                            <th>alamat</th>
                            <th>Sekolah</th>
                            <th>kelas</th>
                            <th>program yang dipilih</th>
                            <th>hari kursus</th>
                            <th>kelas kursus</th>
                            <th>Aksi</th>
                          </tr>

                        </thead>
                        <tbody>
                          @php $no=1; @endphp
                          @forelse ($data_siswa as $siswa)
                            <tr>
                              <td>{{ $no++ }}</td>
                              <td hidden>{{ $siswa->id_siswa }}</td>
                              <td>{{ $siswa->nama_siswa }}</td>

                              <td>{{ $siswa->jenis_kelamin}}</td>
                              <td>{{ $siswa->tempat_lahir}},{{ $siswa->tanggal_lahir}}</td>
                              <td>{{ $siswa->alamat }}</td>
                              <td>{{ $siswa->nama_sekolah}}</td>
                              <td>{{ $siswa->kelas }}</td>
                              <td>{{ $siswa->program_kursus }}</td>
                              <td>{{ $siswa->hari_kursus }}</td>
                              <td>{{ $siswa->jenis_kelas }}</td>
                              <td>
                                <a href="/siswa/{{$siswa->id_siswa}}" class="btn btn-success btn-sm">show</a>
                                <a href="/siswa/{{$siswa->id_siswa}}/edit" class="btn btn-warning btn-sm">Edit</a>

                                <form action="{{ route('siswa.destroy', $siswa->id_siswa) }}" method="post" style="display:inline-block">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn waves-effect waves-dark btn-danger btn-sm" onclick="return confirm('apakah yakin menghapus data?')">Hapus</button>
                                </form>

                              </td>

                            </tr>
                          @empty
                            <div class="alert alert-danger">
                                <strong>Data Masih Kosong !</strong>
                            </div>
                          @endforelse

                          <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Siswa</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                          </button>
                            </div>
                     <div class="modal-body">
                          <form action="/siswa/create" method="POST">
                          {{csrf_field()}}
                            <div class="form-group">
                            <label for="exampleInputEmail1">Nama Siswa</label>
                            <input name="nama_siswa" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama">
                            </div>
                            <div class="form-group">
                              <label for="exampleFormControlTextarea1">Alamat</label>
                              <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>

                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama ayah </label>
                              <input name="ayah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ayah">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama ibu </label>
                              <input name="ibu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ibu">
                            </div>
                          <!-- <div class="form-group">
                            <label for="exampleFormControlSelect1">jenis kelamin</label>
                            <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
                              <option value="L">laki laki</option>
                              <option value="P">perempuan</option>
                            </select>
                          </div> -->




                      </div>
                          <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save</button>
                          </form>
                          </div>
                        </div>
                      </div>
                  </div>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
