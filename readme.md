<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# FYI
ada beberapa branch

## Cara Install

```sh
git clone git@gitlab.com:arifrahman.fauzi/penaameen.git

atau

git clone https://gitlab.com/arifrahman.fauzi/penaameen.git
```
##### install dependencies Laravel

```sh
composer install
```
copy dan rename file .env.example ( intinya pastikan ada file .env )
```
cp .env.example .env
```
!! buat database dulu di phpmyadmin/terserah kasih nama penaameen

buka file .env dengan editor terserah isikan identitas koneksi, username & password menyesuaikan.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=penaameen
DB_USERNAME=root
DB_PASSWORD=
```
jalankan migration
```sh
php artisan migrate
```

kalau ada error `Specified key was too long error`
- cari file `AppServiceProvider.php` di dalam `app/providers` ,di buka dong ...

```
use Illuminate\Support\Facades\Schema;

public function boot()
{
    Schema::defaultStringLength(191);
}
```
- di SAVE !

### YA DI JALANKAN !!!
### Sekian Thx

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
