<?php

// Role permission ada di app/Http/helper.php
// file composer.json "files"
// middleware CheckPermission
// menambah role di helper.php
// make:model <directory>/modelname
Auth::routes();//default auth
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout'); // kalau mau logout pakai route ini

Route::get('/', function () {
    return redirect()->route('login');
});


Route::group(['middleware' => ['auth', 'disablepreventback']], function(){
/*
  !!! semua wajib masuk sini supaya kalau mau akses harus login dulu !!!
  kalau page yang tidak butuh login di taruh di atas !!!
  kalau mau akses page untuk user tertentu pakai middleware checkPermission: namarole
*/
        // User Controllers
        Route::resource('user', 'UserController');
        //routing khusus wilayah
        Route::get('wilayah/kota','GmfControlller@getKota')->name('kota');
        Route::get('wilayah/kecamatan','GmfControlller@getKecamatan');
        Route::get('wilayah/kelurahan', 'GmfControlller@getKelurahan');

        Route::get('/home', 'HomeController@index')->name('home'); //sukses login arahnya ke sini
        Route::post('password/reset', 'ProfileController@resetpassword');
        Route::get('password/form', 'ProfileController@formreset');
        Route::post('profile/avatar', 'ProfileController@avatar')->name('avatar');
        Route::post('profile/{id}', 'ProfileController@biodata');
        //Mitra
        Route::resource('mitra', 'MitraController');

        //GMF
        Route::resource('gmf', 'GmfControlller');
        Route::get('gmf/kota', 'GmfControlller@getKota');
        Route::get('gmf/data', 'GmfControlller@getGmf');


        //siswa
        Route::resource('siswa', 'SiswaController');
        //dewasa
        Route::get('siswa_dewasa','SiswaController@index_dewasa');
        Route::post('/siswa_dewasa','SiswaController@store_dewasa');
        // Route::get('/siswa_dewasa/create','SiswaController@create_dewasa');
        Route::get('/siswa_dewasa/{siswa_dewasa}','SiswaController@show_dewasa');
        Route::delete('/siswa_dewasa/{siswa_dewasa}','SiswaController@destroy_dewasa');
        Route::put('/siswa_dewasa/{siswa_dewasa}','SiswaController@update_dewasa');
        Route::get('/siswa_dewasa/{siswa_dewasa}/edit','SiswaController@edit_dewasa');

        //siswa-end

        Route::group(['middleware' => 'check-permission:admin'], function()
        {
            Route::resource('admin', 'AdminController');
        });
        Route::resource('profile', 'ProfileController');

Route::get('admin',['middleware'=>'check-permission:gmf','uses'=>'HomeController@index']);
//Route::resource('mitra', 'MitraController')->middleware('check-permission:cabang'); // contoh penggunaan middleware
});
