<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = DB::table('view_mitra')->get();
        return view('mitra.kelola-mitra',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data =DB::table('tb_mitra')->get();
      $kota = DB::table('provinsi')->get();
      $daerah = DB::table('kabupaten')->get();
      $jenismitra = DB::table('tb_jenis_mitra')->get();
        return view('mitra.mitra-tambah',['data'=>$data,'wilayah'=>$kota,'kota'=>$daerah,'jenismitra'=>$jenismitra]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $gmf = new Mitra;

      $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ]);
      $avatarName = $request->nik.'_photo'.time().'.'.request()->photo->getClientOriginalExtension(); // mengatur penamaan avatar

      $request->photo->storeAs('gmfphotos',$avatarName); // menyimpan avatar ke folder

      $gmf->photo = $avatarName; // setelah nama di atur di masukkan ke database
      $gmf->nik = $request->nik;
      $gmf->trainer = $request->trainer;
      $gmf->nomor_anggota = $request->nomor_anggota;
      $gmf->nama_mitra = $request->nama_mitra;
      $gmf->jenis_kelamin = $request->jenis_kelamin;
      $gmf->alamat = $request->alamat;
      $gmf->id_provinsi = $request->provinsi;
      $gmf->id_kabupaten = $request->kota;
      $gmf->id_kecamatan = $request->kecamatan;
      $gmf->id_kelurahan = $request->kelurahan;
      $gmf->warga_negara = $request->status_warga;
      $gmf->tgl_lahir = $request->tanggal;
      $gmf->status_kawin = $request->status_kawin;
      $gmf->warga_negara = $request->status_warga;
      $gmf->agama = $request->agama;
      $gmf->suku = $request->suku;
      $gmf->jumlah_anak_l = $request->jumlah_l;
      $gmf->jumlah_anak_p = $request->jumlah_p;
      $gmf->nomor_telp = $request->nomor;
      $gmf->pendidikan = $request->pendidikan;
      $gmf->pendidikan_lain = $request->other;
      $gmf->pekerjaan = $request->pekerjaan;
      $gmf->nip = $request->nip;
      $gmf->npwp = $request->npwp;
      $gmf->status_kepegawaian = $request->status_pegawai;
      $gmf->nama_lembaga = $request->lembaga;
      $gmf->tlp_lembaga = $request->telp_lembaga;
      $gmf->alamat_lembaga = $request->alamat_lembaga;
      $gmf->jabatan = $request->jabatan;
      $gmf->deskripsi = $request->deskripsi;
      $gmf->rt = $request->rt;
      $gmf->rw = $request->rw;
      $gmf->usia = $request->usia;
      $gmf->kota_lahir = $request->kotalahir;
      $gmf->id_jenis_mitra = $request->jenis_mitra;
      $gmf->save();
        return back()->with('status-mitra','data berhasil di simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mitra = DB::table('view_mitra')->where('id_mitra',$id)->first();
         return view ('mitra.mitra-dashboard')->with('data',$mitra);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = DB::table('view_mitra')->where('id_mitra',$id)->get();
      $provinsi = DB::table('provinsi')->get();
      $mitra = Mitra::find($id);
      //dd($mitra);
      $kota = DB::table('kabupaten')->where('IDKabupaten',$mitra->id_kabupaten)->first();
      $kecamatan = DB::table('kecamatan')->where('IDKecamatan',$mitra->id_kecamatan)->first();
      $kelurahan = DB::table('kelurahan')->where('IDKelurahan',$mitra->id_kelurahan)->first();
      $jenismitra = DB::table('tb_jenis_mitra')->get();
      //dd($kecamatan);
        return view('mitra.mitra-edit',['data'=>$data,'wilayah'=>$provinsi,'jenismitra'=>$jenismitra])->with('mitra',$mitra)->with('kabupaten',$kota)->with('kecamatan',$kecamatan)->with('kelurahan',$kelurahan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $gmf = Mitra::find($id);


      $gmf->nik = $request->nik;
      $gmf->trainer = $request->is_trainer;
      $gmf->id_jenis_mitra = $request->jenis_mitra;
      $gmf->nomor_anggota = $request->nomor_anggota;
      $gmf->nomor_sanggar = $request->nomor_sanggar;
      $gmf->nomor_sertifikat = $request->nomor_sertifikat;
      $gmf->nomor_lisensi = $request->nomor_lisensi;
      $gmf->nama_mitra = $request->nama;
      $gmf->jenis_kelamin = $request->jenis_kelamin;
      $gmf->alamat = $request->alamat;
      $gmf->id_provinsi = $request->provinsi;
      $gmf->id_kabupaten = $request->kota;
      $gmf->id_kecamatan = $request->kecamatan;
      $gmf->id_kelurahan = $request->kelurahan;
      $gmf->warga_negara = $request->status_warga;
      $gmf->tgl_lahir = $request->tgl_lahir;
      $gmf->status_kawin = $request->status_kawin;
      $gmf->warga_negara = $request->status_warga;
      $gmf->agama = $request->agama;
      $gmf->suku = $request->suku;
      $gmf->jumlah_anak_l = $request->jumlah_l;
      $gmf->jumlah_anak_p = $request->jumlah_p;
      $gmf->nomor_telp = $request->nomor;
      $gmf->pendidikan = $request->pendidikan;
      $gmf->pendidikan_lain = $request->other;
      $gmf->pekerjaan = $request->pekerjaan;
      $gmf->nip = $request->nip;
      $gmf->npwp = $request->npwp;
      $gmf->status_kepegawaian = $request->status_pegawai;
      $gmf->nama_lembaga = $request->lembaga;
      $gmf->tlp_lembaga = $request->telp_lembaga;
      $gmf->alamat_lembaga = $request->alamat_lembaga;
      $gmf->jabatan = $request->jabatan;
      $gmf->deskripsi = $request->deskripsi;
      $gmf->kota_lahir = $request->kotalahir;
      $gmf->rt = $request->rt;
      $gmf->rw = $request->rw;
      $gmf->usia = $request->usia;

      // $request->validate([
      //       'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048'
      //   ]);
      // $avatarName = $request->nik.'_photo'.time().'.'.request()->photo->getClientOriginalExtension(); // mengatur penamaan avatar
      // $request->photo->storeAs('gmfphotos',$avatarName); // menyimpan avatar ke folder
      //
      // $gmf->photo = $avatarName; // setelah nama di atur di masukkan ke database
      $gmf->save();
        return redirect()->route('mitra.index')->with('success-update','data update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tb_mitra')->where('id_mitra', $id)->delete();
        return back()->with('delete','Data Berhasil Dihapus');
    }
}
