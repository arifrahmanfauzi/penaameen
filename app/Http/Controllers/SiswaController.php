<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data_siswa = DB::table('tb_siswa')->get();
        return view('siswa.siswa-dashboard',['data_siswa'=>$data_siswa]);
    }

    public function index_dewasa()
    {
       $data = DB::table('tb_siswa_dewasa')->get();
       return view('siswa.siswa-dewasa-dashboard',['data'=>$data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
        $data = DB::table('tb_siswa','tb_siswa_dewasa');
        return view('siswa.create',['data'=>$data]);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

      $ortu = DB::table('tb_ortu')->insertGetId([
        'nama_ortu'=>$request->nama_ortu,
        'jenis_kelamin'=>$request->jenis_kelamin_ortu,
        'tempat_lahir'=>$request->tempat_lahir_ortu,
        'tanggal_lahir'=>$request->tanggal_lahir_ortu,
        'umur' =>$request->umur_ortu,
        'alamat' =>$request->alamat_ortu,
        'pendidikan_ayah' =>$request->pendidikan_ayah,
        'pendidikan_ibu' =>$request->pendidikan_ibu,
        'pekerjaan_ayah' =>$request->pekerjaan_ayah,
        'pekerjaan_ibu' =>$request->pekerjaan_ibu,
        'no_telepon' =>$request->no_telp,
        'agama' =>$request->agama_ortu,
        'suku' =>$request->suku_ortu,

          ]);


       $data = DB::table('tb_siswa')->insert([
         'nama_siswa'=>$request->nama,
         'jenis_kelamin'=>$request->jenis_kelamin,
         'tempat_lahir'=>$request->tempat_lahir,
         'tanggal_lahir'=>$request->tanggal_lahir,
         'umur' =>$request->umur,
         'anak_ke' =>$request->anak_ke,
         'bersaudara' =>$request->bersaudara,
         'alamat' =>$request->alamat,
         'nama_sekolah' =>$request->nama_sekolah,
         'kelas' =>$request->kelas,
         'alamat_sekolah' =>$request->alamat_sekolah,
         'agama' =>$request->agama,
         'suku' =>$request->suku,
         'bimbel' =>$request->bimbel,
         'teman_dirumah' =>$request->teman_dirumah,
         'guru_dirumah' =>$request->guru_dirumah,
         'tidur_siang' =>$request->tidur_siang,
         'minat_belajar_dirumah' =>$request->minat_belajar_dirumah,
         'mainan_kesukaan' =>$request->mainan_kesukaan,
         'mengenal_gadget' =>$request->mengenal_gadget,
         'sering_bermain_gadget' =>$request->bermain_gadget,
         'riwayat_penyakit' =>$request->riwayat_penyakit,
         'program_kursus' =>$request->program_kursus,
         'hari_kursus' =>$request->hari_kursus,
         'waktu_kursus' =>$request->waktu_kursus,
         'jenis_kelas' =>$request->jenis_kelas,
         'foto' =>$request->foto,
         'id_ortu'=>$ortu

       ]);

         return back()->with('status-siswa','data berhasil di simpan');
     }

     //dewasa
     public function store_dewasa(Request $request)
     {
         $data = DB::table('tb_siswa_dewasa')->insert([
              'nama_siswa'=>$request->nama,
              'jenis_kelamin'=>$request->jenis_kelamin,
              'tempat_lahir'=>$request->tempat_lahir,
              'tanggal_lahir'=>$request->tanggal_lahir,
              'umur' =>$request->umur,
              'alamat'=>$request->alamat,
              'pendidikan'=>$request->pendidikan,
              'pekerjaan'=>$request->pekerjaan,
              'no_telepon'=>$request->no_telepon,
              'agama' =>$request->agama,
              'suku' =>$request->suku,
              'riwayat_penyakit' =>$request->riwayat_penyakit,
              'program_kursus' =>$request->program_kursus,
              'hari_kursus' =>$request->hari_kursus,
              'waktu_kursus' =>$request->waktu_kursus,
              'jenis_kelas' =>$request->jenis_kelas,
              'foto' =>$request->foto,

            ]);

              return back()->with('status-siswa','data berhasil di simpan');
          }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data_siswa = DB::table('tb_siswa')
           ->join('tb_ortu', 'tb_siswa.id_ortu', '=', 'tb_ortu.id_ortu')
           ->select('tb_siswa.*', 'tb_ortu.nama_ortu')
           ->where('id_siswa',$id)
           ->get();

      return view('siswa.show-siswa',['data'=>$data_siswa]);
    }

    public function show_dewasa($id)
    {
      $data = DB::table('tb_siswa_dewasa')->where('id_siswa',$id)->get();
      return view('siswa.show-siswa-dewasa',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
       $data_siswa = DB::table('tb_siswa')
            ->join('tb_ortu', 'tb_siswa.id_ortu', '=', 'tb_ortu.id_ortu')
            ->select('tb_siswa.*', 'tb_ortu.nama_ortu')
            ->where('id_siswa',$id)
            ->get();
       return view('siswa.edit',['data'=>$data_siswa]);
     }

     public function edit_dewasa($id)
     {
       $data = DB::table('tb_siswa_dewasa')
            ->where('id_siswa',$id)
            ->get();
       return view('siswa.edit-dewasa',['data'=>$data]);
     }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         $data = DB::table('tb_siswa')->where('id_siswa', $id)->update([
           'nama_siswa'=>$request->nama,
           'jenis_kelamin'=>$request->jenis_kelamin,
           'tempat_lahir'=>$request->tempat_lahir,
           'tanggal_lahir'=>$request->tanggal_lahir,
           'umur' =>$request->umur,
           'anak_ke' =>$request->anak_ke,
           'bersaudara' =>$request->bersaudara,
           'alamat' =>$request->alamat,
           'nama_sekolah' =>$request->nama_sekolah,
           'kelas' =>$request->kelas,
           'alamat_sekolah' =>$request->alamat_sekolah,
           'agama' =>$request->agama,
           'suku' =>$request->suku,
           'bimbel' =>$request->bimbel,
           'teman_dirumah' =>$request->teman_dirumah,
           'guru_dirumah' =>$request->guru_dirumah,
           'tidur_siang' =>$request->tidur_siang,
           'minat_belajar_dirumah' =>$request->minat_belajar_dirumah,
           'mainan_kesukaan' =>$request->mainan_kesukaan,
           'mengenal_gadget' =>$request->mengenal_gadget,
           'sering_bermain_gadget' =>$request->bermain_gadget,
           'riwayat_penyakit' =>$request->riwayat_penyakit,
           'program_kursus' =>$request->program_kursus,
           'hari_kursus' =>$request->hari_kursus,
           'waktu_kursus' =>$request->waktu_kursus,
           'jenis_kelas' =>$request->jenis_kelas,
           'foto' =>$request->foto

         ]);
         
         $data = DB::table('tb_ortu')->where('id_ortu', $id)->update([
         'nama_ortu'=>$request->nama_ortu
       ]);

         return redirect()->route('siswa.index')->with('success-update','data update success');
     }

     //dewasa
     public function update_dewasa(Request $request, $id)
     {
         $data = DB::table('tb_siswa_dewasa')->where('id_siswa', $id)->update([
           'nama_siswa'=>$request->nama,
           'jenis_kelamin'=>$request->jenis_kelamin,
           'tempat_lahir'=>$request->tempat_lahir,
           'tanggal_lahir'=>$request->tanggal_lahir,
           'umur' =>$request->umur,
           'alamat'=>$request->alamat,
           'pendidikan'=>$request->pendidikan,
           'pekerjaan'=>$request->pekerjaan,
           'no_telepon'=>$request->no_telepon,
           'agama' =>$request->agama,
           'suku' =>$request->suku,
           'riwayat_penyakit' =>$request->riwayat_penyakit,
           'program_kursus' =>$request->program_kursus,
           'hari_kursus' =>$request->hari_kursus,
           'waktu_kursus' =>$request->waktu_kursus,
           'jenis_kelas' =>$request->jenis_kelas,
           'foto' =>$request->foto,


         ]);

         return redirect()->route('siswa.index')->with('success-update','data update success');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


     public function destroy($id)
     {
       DB::table('tb_siswa')->where('id_siswa', $id)->delete();
       return back()->with('delete','Data Berhasil Dihapus');
     }


   public function destroy_dewasa($id)
   {

       DB::table('tb_siswa_dewasa')->where('id_siswa', $id)->delete();
       return back()->with('delete','Data Berhasil Dihapus');
   }
}
