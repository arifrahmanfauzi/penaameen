<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Gmf;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class GmfControlller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $gmf = Gmf::all();
        return view('gmf.gmf-dashboard',['gmf'=>$gmf]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kota = DB::table('provinsi')->get();
        $daerah = DB::table('kabupaten')->get();

        return view('gmf.gmf-tambah',['wilayah'=>$kota,'kota'=>$daerah]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request->all());
        $gmf = new Gmf;

        $request->validate([
              'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
          ]);
        $avatarName = $request->nik.'_photo'.time().'.'.request()->photo->getClientOriginalExtension(); // mengatur penamaan avatar

        $request->photo->storeAs('gmfphotos',$avatarName); // menyimpan avatar ke folder

        $gmf->photo = $avatarName; // setelah nama di atur di masukkan ke database
        $gmf->nik = $request->nik;
        $gmf->nama = $request->nama;
        $gmf->jenis_kelamin = $request->jenis_kelamin;
        $gmf->alamat = $request->alamat;
        $gmf->id_provinsi = $request->provinsi;
        $gmf->id_kabupaten = $request->kota;
        $gmf->id_kecamatan = $request->kecamatan;
        $gmf->id_kelurahan = $request->kelurahan;
        $gmf->warga_negara = $request->status_warga;
        $gmf->tgl_lahir = $request->tanggal;
        $gmf->status_kawin = $request->status_kawin;
        $gmf->warga_negara = $request->status_warga;
        $gmf->agama = $request->agama;
        $gmf->suku = $request->suku;
        $gmf->jumlah_anak_l = $request->jumlah_l;
        $gmf->jumlah_anak_p = $request->jumlah_p;
        $gmf->nomor_telp = $request->nomor;
        $gmf->pendidikan = $request->pendidikan;
        $gmf->pendidikan_lain = $request->other;
        $gmf->pekerjaan = $request->pekerjaan;
        $gmf->nip = $request->nip;
        $gmf->npwp = $request->npwp;
        $gmf->status_kepegawaian = $request->status_pegawai;
        $gmf->lembaga = $request->lembaga;
        $gmf->tlp_lembaga = $request->telp_lembaga;
        $gmf->alamat_lembaga = $request->alamat_lembaga;
        $gmf->jabatan = $request->jabatan;
        $gmf->deskripsi = $request->deskripsi;





        $gmf->save();

        return back()->with('success','data berhasil di simpan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$gmf = Gmf::find($id)->join('provinsi','tb_gmf.id_provinsi','=','provinsi.IDProvinsi')->get();
        $data = DB::table('tb_gmf')
        ->where('nik',$id)
        ->join('provinsi', 'tb_gmf.id_provinsi', '=', 'provinsi.IDProvinsi')
        ->join('kabupaten', 'tb_gmf.id_kabupaten', '=', 'kabupaten.IDKabupaten')
        ->join('kecamatan', 'tb_gmf.id_kecamatan', '=', 'kecamatan.IDKecamatan')
        ->first();  //pakai first untuk menghindari foreach,
        return view('gmf.gmf-profile')->with('gmf', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kota = DB::table('provinsi')->get();
        $gmf = Gmf::find($id);
        return view('gmf.gmf-edit',['id'=>$id,'wilayah'=>$kota])->with('gmf',$gmf);

        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $gmf = Gmf::find($id);
      $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ]);
      $avatarName = $request->nik.'_photo'.time().'.'.request()->photo->getClientOriginalExtension(); // mengatur penamaan avatar

      $request->photo->storeAs('photo',$avatarName); // menyimpan avatar ke folder

      $gmf->photo = $avatarName; // setelah nama di atur di masukkan ke database
      $gmf->nik = $request->nik;
      $gmf->nama = $request->nama;
      $gmf->jenis_kelamin = $request->jenis_kelamin;
      $gmf->alamat = $request->alamat;
      $gmf->id_provinsi = $request->provinsi;
      $gmf->id_kabupaten = $request->kota;
      $gmf->id_kecamatan = $request->kecamatan;
      $gmf->id_kelurahan = $request->kelurahan;
      $gmf->warga_negara = $request->status_warga;
      $gmf->tgl_lahir = $request->tanggal;
      $gmf->status_kawin = $request->status_kawin;
      $gmf->warga_negara = $request->status_warga;
      $gmf->agama = $request->agama;
      $gmf->suku = $request->suku;
      $gmf->jumlah_anak_l = $request->jumlah_l;
      $gmf->jumlah_anak_p = $request->jumlah_p;
      $gmf->nomor_telp = $request->nomor;
      $gmf->pendidikan = $request->pendidikan;
      $gmf->pendidikan_lain = $request->other;
      $gmf->pekerjaan = $request->pekerjaan;
      $gmf->nip = $request->nip;
      $gmf->npwp = $request->npwp;
      $gmf->status_kepegawaian = $request->status_pegawai;
      $gmf->lembaga = $request->lembaga;
      $gmf->tlp_lembaga = $request->telp_lembaga;
      $gmf->alamat_lembaga = $request->alamat_lembaga;
      $gmf->jabatan = $request->jabatan;
      $gmf->deskripsi = $request->deskripsi;
      $gmf->save();

      return redirect()->route('gmf.index')->with('update','Data Berhasil Di update !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $gmf = Gmf::find($id);
      $gmf->delete();
      return back()->with('delete','data berhasil di hapus');

      $gmf = Gmf::find($id);
      $gmf->delete();
      return back()->with('delete','data berhasil di hapus');

    }
    public function getKota(Request $request){
      $data = DB::table('kabupaten')->where('IDProvinsi',$request->idprovinsi)->pluck('idkabupaten','nama');
      return response()->json($data);
    }
    public function getKecamatan(Request $request){
      $data = DB::table('kecamatan')->where('IDKabupaten',$request->idkota)->pluck('IDKecamatan','nama');
      return response()->json($data);
    }
    public function getKelurahan(Request $request){
      $data = DB::table('kelurahan')->where('IDKecamatan', $request->idkecamatan)->pluck('idkelurahan','nama');
      return response()->json($data);
    }
}
