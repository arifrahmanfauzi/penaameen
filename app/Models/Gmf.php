<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gmf extends Model
{
    protected $table = 'tb_gmf';
    protected $primaryKey = 'nik';
    public $timestamps = false;
}
