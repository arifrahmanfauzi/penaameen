<?php
#InventoryCategoryModel.php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InventoryCategoryModel extends Model
{
    protected $table = 'tb_ortu';

    protected $fillable = [
    	'id_ortu',
    	'nama_ortu',
    	'tempat_lahir'
    ];

    //relasi one to many (Saya memiliki banyak anggota di model .....)
    public function get_siswa(){
    	return $this->hasMany('App\\Model\\SiswaModel', 'kategori', 'id');
    }

}
