$(document).ready(function() {
  //console.log('edit-profile ready');

});
$('#btnEdit').click(function() {
  $('.edit').attr('readonly',false);
  $(this).attr('hidden',true);
  $('#btnSave').attr('hidden',false);
});

$('#btnSave').click(function(event) {
  var formdata = $('#formProfile').serialize();
  var url = $('#formProfile').attr('action');
  var method =  $('#formProfile').attr('method');
  function notify(from, align, icon, type, animIn, animOut){
         $.growl({
             icon: icon,
             title: ' Suskes ',
             message: 'Data berhasil di update',
             url: ''
         },{
             element: 'body',
             type: type,
             allow_dismiss: true,
             placement: {
                 from: from,
                 align: align
             },
             offset: {
                 x: 30,
                 y: 30
             },
             spacing: 10,
             z_index: 999999,
             delay: 2500,
             timer: 1000,
             url_target: '_blank',
             mouse_over: false,
             animate: {
                 enter: animIn,
                 exit: animOut
             },
             icon_type: 'class',
             template: '<div data-growl="container" class="alert" role="alert">' +
             '<button type="button" class="close" data-growl="dismiss">' +
             '<span aria-hidden="true">&times;</span>' +
             '<span class="sr-only">Close</span>' +
             '</button>' +
             '<span data-growl="icon"></span>' +
             '<span data-growl="title"></span>' +
             '<span data-growl="message"></span>' +
             '<a href="#" data-growl="url"></a>' +
             '</div>'
         });
     };
  $('.edit').attr('readonly',true);
    $('#btnEdit').attr('hidden',false);
    var nFrom = 'top';
    var nAlign = 'center';
    var nIcons = 'fa fa-comments';
    var nType = 'success';
    var nAnimIn = 'animated fadeInRight';
    var nAnimOut = 'animated fadeOutRight';
    $.ajax({
      url: url,
      type: method,
      data: formdata,
      success:function(response){

        notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
        //console.log(response['message']);
      },
      error:function(e){
        console.log(e);
      }
    })
});

$('#edit').click(function() {
  $('.ubah').attr('readonly',false);
  $(this).attr('hidden',true);
  $('#save').attr('hidden',false);
});


$('#save').click(function(event) {
  var formdata = $('#formdata').serialize();
  var url = $('#formdata').attr('action');
  var method =  $('#formdata').attr('method');
  function notify(from, align, icon, type, animIn, animOut){
         $.growl({
             icon: icon,
             title: ' Suskes ',
             message: 'Data berhasil di update',
             url: ''
         },{
             element: 'body',
             type: type,
             allow_dismiss: true,
             placement: {
                 from: from,
                 align: align
             },
             offset: {
                 x: 30,
                 y: 30
             },
             spacing: 10,
             z_index: 999999,
             delay: 2500,
             timer: 1000,
             url_target: '_blank',
             mouse_over: false,
             animate: {
                 enter: animIn,
                 exit: animOut
             },
             icon_type: 'class',
             template: '<div data-growl="container" class="alert" role="alert">' +
             '<button type="button" class="close" data-growl="dismiss">' +
             '<span aria-hidden="true">&times;</span>' +
             '<span class="sr-only">Close</span>' +
             '</button>' +
             '<span data-growl="icon"></span>' +
             '<span data-growl="title"></span>' +
             '<span data-growl="message"></span>' +
             '<a href="#" data-growl="url"></a>' +
             '</div>'
         });
     };
  $('.ubah').attr('readonly',true);
    $('#edit').attr('hidden',false);
    $('#save').attr('hidden',true);
    var nFrom = 'top';
    var nAlign = 'center';
    var nIcons = 'fa fa-comments';
    var nType = 'success';
    var nAnimIn = 'animated fadeInRight';
    var nAnimOut = 'animated fadeOutRight';
  $.ajax({
    url: url,
    type: method,
    data: formdata,
    success:function(response){

      notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
      //console.log(response['message']);
    },
    error:function(e){
      console.log(e);
    }
  })
});
